// NextJs Config;
const path = require('path');
const withLess = require('@zeit/next-less');
const withImages = require('next-images');
const autoprefixer = require('autoprefixer');
const withFonts = require('next-fonts');

const ENV = process.env.NODE_ENV;
const PROD = ENV === 'production';

module.exports = withFonts(withImages(withLess({
  distDir: "_next",
  generateBuildId: async () => {
    if (process.env.BUILD_ID) {
      return process.env.BUILD_ID;
    } else{
      return `${new Date().getTime()}`;
    }
  },
  enableSvg: true,
  webpack: config => {
    config.resolve.alias['components'] = path.resolve(__dirname, './components');
    config.resolve.alias['styles'] = path.resolve(__dirname, './styles');
    config.resolve.alias['utils'] = path.resolve(__dirname, './utils');
    config.resolve.alias['mockData'] = path.resolve(__dirname, './mockData');

    config.module.rules.push({
      test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          publicPath: '/_next/static/'
        }
      }
    });

    return config;
  },
  exportTrailingSlash: true,
  exportPathMap: function() {
    return {
      '/': { page: '/' },
      '/about': { page: '/about' }
    };
  },
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: "[local]___[hash:base64:5]",
  }
})));
