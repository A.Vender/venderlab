import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

// Components
import HeadBanner from './components/HeadBanner';

import AdvantageGroups from './components/AdvantageGroups';
import ServicesGroups from '../services/components/ServicesGroups';
import TariffsGroups from '../tariffs/components/TariffsGroups';
import CasesGroups from '../cases/components/CasesGroups';
import {
  Layout,
  Container,
  StaticText,
  Footer,
  TextAnimation,
  Meta,
} from 'components';

// Utils
import {
  setSessionStorage,
  getFromSessionStorage
} from 'utils/storage';
import { getCoords } from 'utils/getCoords';

// Data
import baseData from 'mockData/base';
import advantageData from 'mockData/advantage';
import serviceData from 'mockData/service';
import tariffsData from 'mockData/tariffs';
import casesData from 'mockData/cases';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class Index extends Component {
  state = {
    serviceAnim: false,
    tariffsAnim: false,
    casesAnim: false,
    advantageAnim: false,
    footerAnim: false,
    headAnim: false,
    isGreeting: false,
  };

  componentDidMount() {
    const greetingFromStorage = getFromSessionStorage('greeting');
    if (!greetingFromStorage) {
      this.setState({
        isGreeting: true
      });
    } else {
      this.setState({
        headAnim: true
      });
    }
  }

  completeAnimation = () => {
    this.setState({
      isGreeting: false,
    }, () => {
      this.setState({
        headAnim: true,
      });
    });

    setSessionStorage('greeting', true);
  };

  onScroll = () => {
    const { serviceAnim, casesAnim, footerAnim, tariffsAnim, advantageAnim } = this.state;

    const advantageCoords = getCoords(this.advantage);
    const serviceCoords = getCoords(this.services);
    const tariffsCoords = getCoords(this.tariffs);
    const caseCoords = getCoords(this.cases);
    const footersCoords = getCoords(this.footer);

    const advantage = !advantageAnim && advantageCoords < 0.85 || advantageAnim;
    const service = !serviceAnim && serviceCoords < 0.85 || serviceAnim;
    const tariffs = !tariffsAnim && tariffsCoords < 0.85 || tariffsAnim;
    const cases = !casesAnim && caseCoords < 0.85 || casesAnim;
    const footer = !footerAnim && footersCoords < 0.85 || footerAnim;

    this.setState({
      advantageAnim: advantage,
      serviceAnim: service,
      tariffsAnim: tariffs,
      casesAnim: cases,
      footerAnim: footer,
    });
  };

  render() {
    const {
      serviceAnim,
      tariffsAnim,
      advantageAnim,
      casesAnim,
      footerAnim,
      headAnim,
      isGreeting,
    } = this.state;

    const { service, base, cases, tariffs, advantage } = this.props.data;

    return (
      <Layout>
        <Meta
          title="VENDERLAB — веб-дизайн и разработка сайтов под ключ в Москве"
          description="Создадим сайт для вашего бизнеса с нуля под ключ: веб-дизайн, разработка, продвижение, поддержка. Качественно от профессионалов."
          keywords="веб-дизайн и разработка сайтов под ключ в Москве, разработка веб сайтов html, создание и поддержка сайтов"
          canonical="https://vdlb.ru"
        />
        {isGreeting ? (
          <div className={cx('greeting')}>
            <TextAnimation.Modern
              type="inOut"
              animationName="greeting"
              className={cx('logo')}
              text={StaticText.projectName}
              callback={this.completeAnimation}
            />
          </div>
        ) : (
          <React.Fragment>
            <Container.Base onScroll={this.onScroll}>
              <HeadBanner {...base.heading} play={headAnim} />

              <div ref={advantage => { this.advantage = advantage; }}>
                <AdvantageGroups
                  title={advantage.title}
                  advantage={advantage.entries}
                  play={advantageAnim}
                />
              </div>

              <div ref={services => { this.services = services; }}>
                <ServicesGroups
                  title={service.title}
                  service={service.entries}
                  play={serviceAnim}
                />
              </div>

              <div ref={cases => { this.cases = cases; }}>
                <CasesGroups
                  tabs={cases.tabs}
                  design={cases?.entries?.design?.slice(0, 3)}
                  site={cases?.entries?.site?.slice(0, 3)}
                  content={cases?.entries?.content?.slice(0, 3)}
                  play={casesAnim}
                />
              </div>

              <div ref={tariffs => { this.tariffs = tariffs; }}>
                <TariffsGroups
                  title={tariffs.title}
                  tariffs={tariffs.entries}
                  play={tariffsAnim}
                />
              </div>

              <div ref={footer => { this.footer = footer; }}>
                <Footer {...base.footer} play={footerAnim} />
              </div>
            </Container.Base>
          </React.Fragment>
        )}
      </Layout>
    );
  }
}

Index.propTypes = {
  data: PropTypes.shape({
    base: PropTypes.object,
    cases: PropTypes.object,
    service: PropTypes.object,
    tariffs: PropTypes.object,
  })
};

Index.getInitialProps = () => {
  return {
    data: {
      base: baseData || {},
      cases: casesData || {},
      service: serviceData || {},
      tariffs: tariffsData || {},
      advantage: advantageData || {},
    }
  };
};

export default Index;
