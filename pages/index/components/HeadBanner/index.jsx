import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  Text,
  Button,
  StaticText,
  BlockAnimation,
} from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const HeadBanner = ({
  subTitle,
  title,
  description,
  buttonService,
  buttonOrder,
  play,
}) => {
  return (
    <div className={cx('headBanner')}>
      <div className={cx('wrapper')}>
        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={100}
          duration={700}
          translateY={30}
          animationId="headBanner"
        >
          <Text
            className={cx('subTitle')}
            text={subTitle}
          />
        </BlockAnimation>
        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={(180)}
          duration={700}
          translateY={30}
          animationId="headBanner"
        >
          <h1
            className={cx('title')}
            dangerouslySetInnerHTML={{ __html: title }}
          />
        </BlockAnimation>

        {description && (
          <BlockAnimation
            play={play}
            skewY={-0.5}
            delay={(260)}
            duration={700}
            translateY={30}
            animationId="headBanner"
          >
            <h2
              className={cx('description')}
              dangerouslySetInnerHTML={{ __html: description }}
            />
          </BlockAnimation>
        )}

        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={230}
          duration={700}
          translateY={30}
          animationId="headBannerButtonsGroup"
        >
          <div className={cx('buttonsGroup')}>
            <Button.Base
              link={(buttonService || {}).link}
              buttonText={(buttonService || {}).name}
            />
            <Button.Base
              target="_blank"
              link={(buttonOrder || {}).link}
              buttonText={(buttonOrder || {}).name}
            />
          </div>
        </BlockAnimation>
      </div>

      <div className={cx('scroll')}>
        <span>{StaticText.scroll}</span>
      </div>
    </div>
  );
};

HeadBanner.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  buttonService: PropTypes.object,
  buttonOrder: PropTypes.object,
  play: PropTypes.bool,
};

export default HeadBanner;
