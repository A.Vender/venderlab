import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  Grid,
  ProductCard,
  Container,
  BlockAnimation,
} from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const AdvantageGroups = ({ title, advantage, play }) => (
  <Container.Block
    play={play}
    animationId="advantage"
    title={title}
  >
    <ul className={cx('gridList')}>
      {advantage?.map((item, index) => (
        <li>
          <BlockAnimation
            play={play}
            key={index}
            duration={900}
            delay={(120*index)}
            animationId={`advantageCardList-${index}`}
            className={cx('advantageCardsItem')}
          >
              <div className={cx('advantage')}>
                <div className={cx('title')}>{item.title}</div>
                <div className={cx('text')}>{item.text}</div>
              </div>
          </BlockAnimation>
        </li>
      ))}
    </ul>
  </Container.Block>
);

export default AdvantageGroups;
