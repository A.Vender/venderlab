import React from 'react';
import App from 'next/app';

class RootApp extends App {
  render() {
    const { Component, pageProps } = this.props;
    return (
      <React.Fragment>
        <Component {...pageProps} />
        <style jsx global>{`
        html {
            font-family: 'Roboto', sans-serif;
          }

         body {
          margin: 0;
          padding: 0;
         }
         
        @font-face {
          font-family: "Universe";
          src: url("/static/fonts/Universe/Universe.ttf");
        }
      `}
        </style>
      </React.Fragment>
    )
  }
}

export default RootApp;
