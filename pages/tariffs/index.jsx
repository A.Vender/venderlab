import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

// Components
import TariffsGroups from './components/TariffsGroups';
import {
  Layout,
  StaticText,
  Footer,
  Container,
  Text,
  BlockAnimation,
  Meta,
} from 'components';

// Utils
import { getCoords } from 'utils/getCoords';

// Data
import baseData from '../../mockData/base';
import tariffsData from '../../mockData/tariffs';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class Prices extends Component {
  state = {
    tariffsAnim: false,
    footerAnim: false,
    descriptionAnim: false,
  };

  componentDidMount() {
    this.setState({ tariffsAnim: true })
  }

  onScroll = () => {
    const { footerAnim, descriptionAnim } = this.state;

    const footersCoords = getCoords(this.footer);
    const descriptionCoords = getCoords(this.description);

    const footer = !footerAnim && footersCoords < 0.85 || footerAnim;
    const textDescriptionAnim = !descriptionAnim && descriptionCoords < 0.85 || descriptionAnim;

    this.setState({
      footerAnim: footer,
      descriptionAnim: textDescriptionAnim,
    });
  };

  render() {
    const { tariffsAnim, footerAnim, descriptionAnim } = this.state;
    const { tariffs, base } = this.props.data;

    return (
      <Layout>
        <Meta
          title="Стоимость создания сайта под ключ в Москве: стоимость разработки сайта в VENDERLAB"
          description="Стоимость создания сайта под ключ в Москве от веб-студии Venderlab (Вендерлаб). Адекватные цены и качественный результат. Бонус для для новых клиентов - 1 месяц техобслуживания сайта бесплатно."
          keywords="создание сайта цены, стоимость разработки сайта, в москве, под ключ, лендинг, посадочная страница, сайт-визитка"
        />
        <Container.Base onScroll={this.onScroll}>
          <TariffsGroups
            title={tariffs.title}
            tariffs={tariffs.entries}
            play={tariffsAnim}
          />

          <Container.Block>
            <BlockAnimation
              play={descriptionAnim}
              skewY={0}
              delay={(200)}
              duration={600}
              translateY={30}
              animationId="textAbout"
            >
              <div ref={description => { this.description = description; }} className={cx('description')}>
                <Text text={tariffs.textAbout}/>
              </div>
            </BlockAnimation>
          </Container.Block>

          <div ref={footer => { this.footer = footer; }}>
            <Footer {...base.footer} play={footerAnim} />
          </div>
        </Container.Base>
      </Layout>
    );
  }
}

Prices.propTypes = {
  data: PropTypes.shape({
    base: PropTypes.object,
    pricesData: PropTypes.object,
  })
};

Prices.getInitialProps = () => {
  return {
    data: {
      base: baseData,
      tariffs: tariffsData,
    }
  };
};

export default Prices;
