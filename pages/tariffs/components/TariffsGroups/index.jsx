import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  Grid,
  ProductCard,
  Container,
  BlockAnimation,
} from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const TariffsGroups = ({ title, tariffs, play }) => (
  <Container.Block
    play={play}
    animationId="tariffs"
    title={title}
  >
    <div className={cx('gridList')}>
      <Grid.Row>
        {tariffs.map((item, index) => (
          <BlockAnimation
            play={play}
            key={index}
            duration={900}
            delay={(120*index)}
            animationId={`tariffsCardList-${index}`}
            className={cx('tariffsCardsItem')}
          >
            <ProductCard index={(index+1)} {...item} />
          </BlockAnimation>
        ))}
      </Grid.Row>
    </div>
  </Container.Block>
);

TariffsGroups.defaultProps = {
  tariffs: []
};

TariffsGroups.propTypes = {
  title: PropTypes.string,
  tariffs: PropTypes.array,
  play: PropTypes.bool,
};

export default TariffsGroups;
