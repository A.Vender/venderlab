import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';

// Components
import {
  Layout,
  StaticText,
  Container,
  BlockAnimation,
  Button,
  Text,
  Meta,
} from 'components';

// Data
import baseData from '../../mockData/base';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class Contacts extends Component {
  state = {
    contactsAnim: false
  };

  componentDidMount() {
    this.setState({ contactsAnim: true })
  }

  render() {
    const { contactsAnim } = this.state;
    const { contacts } = this.props.data;

    return (
      <Layout>
        <Meta
          title="Контактная информация - веб-студия VENDERLAB в Москве"
          description="Контактная информация веб-студии Venderlab (Вендерлаб) в Москве. Свяжитесь с нами, чтобы заказать сайт +7 (916) 042 8723."
          keywords="контактная инфформация, контактные данные, venderlab, вендерлаб"
        />
        <Container.Base>
          <Container.Block
            play={contactsAnim}
            animationId="contacts"
            title={contacts.title}
          >
            <BlockAnimation
              play={contactsAnim}
              skewY={-0.5}
              delay={230}
              duration={700}
              translateY={30}
              animationId="contactsList"
            >
              <ul className={cx('contactsList')}>
                {contacts.list.map(item => (
                  <li key={item.type}>
                    {item.type === 'email' && (
                      <React.Fragment>
                        <Text
                          text={`${item.name}:&nbsp`}
                        />
                        <Text
                          className={cx('textColor')}
                          text={`<a href='mailto:${item.value}'>${item.value}</a>`}
                        />
                      </React.Fragment>
                    )}

                    {item.type === 'text' && (
                      <React.Fragment>
                        <Text
                          text={`${item.name}:&nbsp`}
                        />
                        <Text
                          className={cx('textColor')}
                          text={item.value}
                        />
                      </React.Fragment>
                    )}

                    {item.type === 'link' && (
                      <Text
                        className={cx('textLink')}
                        text={`<a target="_blank" href='${item.value}'>${item.name}</a>`}
                      />
                    )}

                    {item.type === 'button' && (
                      <Button.Base
                        target="_blank"
                        link={item.value}
                        buttonText={item.name}
                      />
                    )}
                  </li>
                ))}
              </ul>
            </BlockAnimation>
          </Container.Block>
        </Container.Base>
      </Layout>
    );
  }
}

Contacts.propTypes = {
  data: PropTypes.shape({
    contacts: PropTypes.object,
  })
};

Contacts.getInitialProps = () => {
  return {
    data: {
      contacts: baseData.footer.contacts,
    }
  };
};

export default Contacts;
