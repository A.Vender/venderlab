import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  Container,
  Tabs,
  CasePreview,
  BlockAnimation,
} from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const CasesGroups = ({ titleAnim, tabs, design, site, content, play, activeTab }) => (
  <Container.Block
    play={play}
    animationId="case"
    title="Кейсы"
  >
    <Tabs defaultIndex={activeTab} tabs={tabs}>
      <Container.ColumnList className={cx('containerColumnList')}>
        {design.map((item, index) => (
          <BlockAnimation
            play={play}
            key={index}
            duration={900}
            delay={(500*index)}
            animationId={`caseCardList-${index}`}
          >
            <CasePreview {...item} index={(index+1)} />
          </BlockAnimation>
        ))}
      </Container.ColumnList>

      <Container.ColumnList className={cx('containerColumnList')}>
        {site.map((item, siteIndex) => (
          <CasePreview
            key={siteIndex}
            {...item}
            index={(siteIndex+1)}
          />
        ))}
      </Container.ColumnList>

      <Container.ColumnList className={cx('containerColumnList')}>
        {content && content.map((item, contentIndex) => (
          <CasePreview
            key={contentIndex}
            {...item}
            index={(contentIndex+1)}
          />
        ))}
      </Container.ColumnList>
    </Tabs>
  </Container.Block>
);

CasesGroups.defaultProps = {
  content: [],
  design: [],
  site: [],
};

CasesGroups.propTypes = {
  tabs: PropTypes.array,
  design: PropTypes.array,
  site: PropTypes.array,
  content: PropTypes.array,
  play: PropTypes.bool,
};

export default CasesGroups;
