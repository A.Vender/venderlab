import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import CasesGroups from './components/CasesGroups';
import {
  Layout,
  StaticText,
  Footer,
  Container,
  Meta,
} from 'components';

// Utils
import { getCoords } from 'utils/getCoords';

// Data
import baseData from 'mockData/base';
import casesData from 'mockData/cases';

class Cases extends Component {
  state = {
    casesAnim: false,
    footerAnim: false,
    titleAnim: false
  };

  componentDidMount() {
    this.setState({
      casesAnim: true,
      titleAnim: true,
    })
  }

  onScroll = () => {
    const { footerAnim } = this.state;

    const footersCoords = getCoords(this.footer);
    const footer = !footerAnim && footersCoords < 0.85 || footerAnim;

    this.setState({ footerAnim: footer });
  };

  render() {
    const { casesAnim, footerAnim, titleAnim } = this.state;
    const { cases, base } = this.props.data;

    return (
      <Layout>
        <Meta
          title="Кейсы по веб-дизайну и разработке сайтов: примеры работ по созданию сайтов от VENDERLAB"
          description="Примеры работ от веб-студии Venderlab (Вендерлаб): веб-дизайн и разработка сайтов-визиток, landing page, многостраничных сайтов."
          keywords="создание сайтов кейсы, разработка сайтов примеры"
        />
        <Container.Base onScroll={this.onScroll}>
          <CasesGroups
            titleAnim={titleAnim}
            play={casesAnim}
            tabs={cases.tabs}
            design={cases.entries.design}
            site={cases.entries.site}
            content={cases.entries.content}
          />

          <div ref={footer => { this.footer = footer; }}>
            <Footer {...base.footer} play={footerAnim} />
          </div>
        </Container.Base>
      </Layout>
    );
  }
}

Cases.propTypes = {
  data: PropTypes.shape({
    base: PropTypes.object,
    cases: PropTypes.object,
  })
};

Cases.getInitialProps = () => {
  return {
    data: {
      base: baseData,
      cases: casesData,
    }
  };
};

export default Cases;
