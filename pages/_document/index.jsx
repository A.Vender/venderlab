import Document, { Html, Head, Main, NextScript } from 'next/document'
import React from "react";

export default class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <meta charSet="utf-8" />
          <link
            href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;700&display=swap"
            rel="stylesheet"
          />
          <link
            rel="preload"
            href="/static/fonts/Universe/Universe.ttf"
            as="font"
            crossOrigin=""
          />
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169213831-1"></script>
          <script
            dangerouslySetInnerHTML={{
              __html: `
                <!-- Global site tag (gtag.js) - Google Analytics -->
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-169213831-1');
              `
            }}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
              <script type="text/javascript" >
                 (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                 m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                 (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

                 ym(86892490, "init", {
                      clickmap:true,
                      trackLinks:true,
                      accurateTrackBounce:true,
                      webvisor:true
                 });
              </script>
              `
            }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
          <noscript>
            <img src="https://mc.yandex.ru/watch/86892490" style={{
              position: 'absolute',
              left: '-9999px',
            }} alt="" />
          </noscript>
        </body>
      </Html>
    )
  }
}
