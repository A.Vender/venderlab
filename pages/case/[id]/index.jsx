import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

// Component
import {
  BlockAnimation,
  Container,
  Button,
  Footer,
  Picture,
  Text,
  Meta,
} from 'components';

// Utils
import { getCoords } from 'utils/getCoords';

// Data
import base from 'mockData/base';
import cases from 'mockData/cases';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class Case extends Component {
  state = {
    caseAnim: false,
    footerAnim: false,
  };

  componentDidMount() {
    this.setState({ caseAnim: true });
  }

  onScroll = () => {
    const { footerAnim } = this.state;

    const footersCoords = getCoords(this.footer);
    const footer = !footerAnim && footersCoords < 0.85 || footerAnim;

    this.setState({ footerAnim: footer });
  };

  render() {
    const { footerAnim, caseAnim } = this.state;
    const { data } = this.props;

    return (
      <Fragment>
        <Meta
          title={data?.seo?.title}
          description={data?.seo?.description}
          keywords={data?.seo?.keywords}
        />
        <Container.Base onScroll={this.onScroll}>
          <Container.Block
            play={caseAnim}
            animationId={`case-${data.id}`}
            title={data.title}
          >
            <BlockAnimation
              play={caseAnim}
              duration={600}
              delay={(300)}
              skewY={0}
              animationId="caseDescription"
            >
              <Text text={data.description} />
            </BlockAnimation>

            <BlockAnimation
              play={caseAnim}
              duration={600}
              delay={(400)}
              skewY={0}
              animationId="caseImageList"
            >
              <div className={cx('case')}>
                {data.images.map((item, index) => (
                  <Picture lazy key={index} src={item} />
                ))}
              </div>

              {data.copyright && (
                <ul  className={cx('copyright')}>
                  {data.copyright.map(item => (
                    <li>
                      <Text className={cx('text')} text={`Источник фото: ${item}`} />
                    </li>
                  ))}
                </ul>
              )}

              <div className={cx('nextCase')}>
                <Button.Base
                  buttonText={data.nextButtonText}
                  link={data.next}
                />
              </div>
            </BlockAnimation>
          </Container.Block>
          <div ref={footer => { this.footer = footer; }}>
            <Footer {...base.footer} play={footerAnim} />
          </div>
        </Container.Base>
      </Fragment>
    );
  }
}

Case.propTypes = {
  case: PropTypes.shape({
    id: PropTypes.string,
    title: PropTypes.string,
    copyright: PropTypes.array,
    nextButtonText: PropTypes.string,
    next: PropTypes.next,
  })
};

Case.getInitialProps = ({ query }) => {
  const { id } = query;
  const currentCase = cases.entries.design.filter(item => item.id === id)[0];
  return { data: currentCase };
};

export default Case;
