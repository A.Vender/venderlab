import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';

// Components
import {
  Layout,
  StaticText,
  Footer,
  Container,
  Text,
  Meta,
  BlockAnimation, Button,
} from 'components';

// Utils
import { getCoords } from 'utils/getCoords';

// Data
import baseData from 'mockData/base';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class About extends Component {
  state = {
    aboutAnim: false,
    footerAnim: false,
  };

  componentDidMount() {
    this.setState({ aboutAnim: true })
  }

  onScroll = () => {
    const { footerAnim } = this.state;

    const footersCoords = getCoords(this.footer);
    const footer = !footerAnim && footersCoords < 0.85 || footerAnim;

    this.setState({ footerAnim: footer });
  };

  render() {
    const { aboutAnim, footerAnim } = this.state;
    const { base } = this.props.data;

    return (
      <Layout>
        <Meta
          title="Веб-студия по созданию сайтов VENDERLAB в Москве - О нас"
          description="Команда профессионалов с 10-летним опытом работы. Веб-студия Venderlab (Вендерлаб) предлагает качественные услуги: веб-дизайн, веб-разработка, копирайтинг, SEO-продвижение, техподдержка."
          keywords="веб-студия, создание сайтов, разработка, программирование, веб-дизайн, копирайтинг, venderlab, вендерлаб"
        />
        <Container.Base onScroll={this.onScroll}>
          <Container.Block
            play={aboutAnim}
            animationId="about"
            title={StaticText.aboutPageTitle}
          >
            <BlockAnimation
              play={aboutAnim}
              skewY={0}
              delay={(200)}
              duration={600}
              translateY={30}
              animationId="textAbout"
            >
              <Fragment>
                <h1 className={cx('title')} dangerouslySetInnerHTML={{ __html: base?.title }}/>
                <div className={cx('description')}>
                  <Text text={base.textAbout}/>
                </div>
              </Fragment>
            </BlockAnimation>

            <BlockAnimation
              play={aboutAnim}
              skewY={0}
              delay={(400)}
              duration={800}
              translateY={30}
              animationId="buttonContact"
            >
              <Button.Base
                link={(base.heading.buttonOrder || {}).link}
                buttonText={(base.heading.buttonOrder || {}).name}
              />
            </BlockAnimation>
          </Container.Block>

          <div ref={footer => { this.footer = footer; }}>
            <Footer {...base.footer} play={footerAnim} />
          </div>
        </Container.Base>
      </Layout>
    );
  }
}

About.propTypes = {
  data: PropTypes.shape({
    base: PropTypes.object,
  })
};

About.getInitialProps = () => {
  return {
    data: {
      base: baseData,
    }
  };
};

export default About;
