import React, { Component } from 'react';
import Head from 'next/head';

// Components
import {
  Layout,
  StaticText,
  Container,
  BlockAnimation,
  Button,
  Text,
} from 'components';

// Data
import baseData from '../../mockData/base';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class Legal extends Component {
  state = {
    contactsAnim: false
  };

  componentDidMount() {
    this.setState({ contactsAnim: true })
  }

  render() {
    const { contactsAnim } = this.state;
    const { legal } = this.props.data;

    console.log('LEGAL ... ', legal);

    return (
      <Layout>
        <Head>
          <title>{StaticText.contactsPageTitle}</title>
        </Head>
        <Container.Base>
          <Container.Block>
            <div className={cx('legalTitle')}>{legal.title}</div>
            <ul className={cx('legalList')}>
                {legal.list.map(item => (
                  <li key={item.type}>
                    {item.type === 'email' && (
                      <React.Fragment>
                        <Text
                          text={`${item.name}:&nbsp`}
                        />
                        <Text
                          className={cx('textColor')}
                          text={`<a href='mailto:${item.value}'>${item.value}</a>`}
                        />
                      </React.Fragment>
                    )}

                    {item.type === 'text' && (
                      <React.Fragment>
                        <Text
                          text={`${item.name}:&nbsp`}
                        />
                        <Text
                          className={cx('textColor')}
                          text={item.value}
                        />
                      </React.Fragment>
                    )}

                    {item.type === 'link' && (
                      <Text
                        className={cx('textLink')}
                        text={`<a target="_blank" href='${item.value}'>${item.name}</a>`}
                      />
                    )}

                    {item.type === 'button' && (
                      <Button.Base
                        target="_blank"
                        link={item.value}
                        buttonText={item.name}
                      />
                    )}
                  </li>
                ))}
            </ul>
          </Container.Block>
        </Container.Base>
      </Layout>
    );
  }
}

Legal.getInitialProps = () => {
  return {
    data: {
      legal: baseData?.legal,
    }
  };
};

export default Legal;
