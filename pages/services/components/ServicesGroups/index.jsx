import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  Grid,
  ProductCard,
  Container,
  BlockAnimation,
} from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const ServicesGroups = ({ titleAnim, title, service, play }) => (
  <Container.Block
    play={play}
    animationId="service"
    title={title}
  >
    <BlockAnimation
      play={titleAnim}
      skewY={0}
      delay={(200)}
      duration={600}
      translateY={30}
      animationId="textAbout"
    >
      <h1 className={cx('title')}>Наши сайты растут вместе с вашим бизнесом</h1>
    </BlockAnimation>
    <div className={cx('gridList')}>
      <Grid.Row>
        {service.map((item, index) => (
          <BlockAnimation
            play={play}
            key={index}
            duration={900}
            delay={(120*index)}
            animationId={`serviceCardList-${index}`}
            className={cx('serviceCardsItem')}
          >
            <ProductCard
              {...item}
              index={(index+1)}
            />
          </BlockAnimation>
        ))}
      </Grid.Row>
    </div>
  </Container.Block>
);

ServicesGroups.defaultProps = {
  service: []
};

ServicesGroups.propTypes = {
  title: PropTypes.string,
  service: PropTypes.array,
  play: PropTypes.bool,
};

export default ServicesGroups;
