import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import ServicesGroups from './components/ServicesGroups';
import {
  Layout,
  Footer,
  Container,
  Meta,
} from 'components';

// Utils
import { getCoords } from 'utils/getCoords';

// Data
import baseData from "../../mockData/base";
import serviceData from "../../mockData/service";

class Services extends Component {
  state = {
    serviceAnim: false,
    footerAnim: false,
    titleAnim: false,
  };

  componentDidMount() {
    this.setState({
      serviceAnim: true,
      titleAnim: true
    })
  }

  onScroll = () => {
    const { footerAnim } = this.state;

    const footersCoords = getCoords(this.footer);
    const footer = !footerAnim && footersCoords < 0.85 || footerAnim;

    this.setState({ footerAnim: footer });
  };

  render() {
    const { serviceAnim, footerAnim, titleAnim } = this.state;
    const { service, base } = this.props.data;

    return (
      <Layout>
        <Meta
          title="Услуги по созданию веб-сайтов в Москве: услуги веб-студии VENDERLAB"
          description="Веб-студия Venderlab (Вендерлаб) предлагает услуги по созданию веб-сайтов. Веб-дизайн, веб-разработка, копирайтинг, настройка seo, техподдержка."
          keywords="услуги по созданию и продвижению сайтов, создание сайта стоимость услуги, создание сайтов услуга москва"
        />
        <Container.Base onScroll={this.onScroll}>
          <ServicesGroups
            titleAnim={titleAnim}
            title={service.title}
            service={service.entries}
            play={serviceAnim}
          />
          <div ref={footer => { this.footer = footer; }}>
            <Footer {...base.footer} play={footerAnim} />
          </div>
        </Container.Base>
      </Layout>
    );
  }
}

Services.propTypes = {
  data: PropTypes.shape({
    base: PropTypes.object,
    service: PropTypes.object,
  })
};

Services.getInitialProps = () => {
  return {
    data: {
      base: baseData,
      service: serviceData,
    }
  };
};

export default Services;
