import React from 'react';
import PropTypes from 'prop-types';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Logo = ({ className, type }) => (
  <span className={cx('logo', className, type)} />
);

Logo.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
};

export default Logo;
