import React from 'react';
import PropTypes from 'prop-types';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Title = ({ level, children, className }) => {
  const Comp = `h${level}`;

  return (
    <Comp className={cx('title', className)}>{children}</Comp>
  );
};

Title.propTypes = {
  level: PropTypes.number
};

export default Title;
