import React from 'react';
import PropTypes from 'prop-types';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Text = ({ text, className, tag }) => {
  const Tag = tag || 'div';

  return (
    <Tag
      className={cx('text', className)}
      dangerouslySetInnerHTML={{ __html: text }}
    />
  );
};

Text.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
};

export default Text;
