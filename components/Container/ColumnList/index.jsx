import React from 'react';
import PropTypes from 'prop-types';

// Components
import { Button } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const ColumnList = ({ children, buttonText, onClick, className }) => (
  <div className={cx('columnList', className)}>
    <ul>
      {children.map((block, index) => (
        <li key={index}>{block}</li>
      ))}
    </ul>
    {onClick && (
      <div className={cx('button')}>
        <Button.Base
          buttonText={buttonText}
          onClick={onClick}
        />
      </div>
    )}
  </div>
);

ColumnList.defaultProps = {
  buttonText: 'Перейти',
};

ColumnList.propTypes = {
  children: PropTypes.array,
  buttonText: PropTypes.string,
  className: PropTypes.string,
  link: PropTypes.string,
  onClick: PropTypes.func,
};

export default ColumnList;
