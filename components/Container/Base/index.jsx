import React from 'react';
import PropTypes from 'prop-types';

// Components
import { HeaderBar } from 'components';
import { Scrollbars } from 'react-custom-scrollbars';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

/**
 * Контейнер со ScrollBar'ом;
 * -----------------------------------------------------
 * @param children - Контент, который подставится во внутрь ScrollBar'a;
 * @param onScroll - Callback следит за прокруткой страницы;
 * @param height - Высота контента - значение переданное в height;
 * @param dataType - Значение атрибута data-type;
 * @param className - Дополнительный класс;
 * @param forwardedRef - Ref Компонента;
 */
const Base = ({
  children,
  className,
  onScroll,
  height,
  dataType,
  forwardedRef
}) => (
  <Scrollbars
    ref={forwardedRef}
    data-type={dataType}
    autoHeightMin="100vh"
    className={className}
    onScroll={onScroll}
    autoHeight
  >
    <HeaderBar />
    <div className={cx('content')}>
      {children}
    </div>
  </Scrollbars>
);

Base.defaultProps = {
  forwardedRef: () => {},
};

Base.propTypes = {
  onScroll: PropTypes.func,
  height: PropTypes.string,
  dataType: PropTypes.string,
  forwardedRef: PropTypes.func, // Function set ref
};

export default Base;
