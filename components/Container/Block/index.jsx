import React from 'react';
import PropTypes from 'prop-types';

// Components
import { TextAnimation } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Block = ({ animationId, children, title, play }) => (
  <div className={cx('block')}>
    <TextAnimation.Sunny
      play={play}
      animationId={animationId}
      className={cx('title')}
      text={title}
    />
    {children}
  </div>
);

Block.propTypes = {
  animationId: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  play: PropTypes.bool,
};

export default Block;
