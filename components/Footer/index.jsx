import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import { Button, Text, BlockAnimation } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Footer = ({ workWithUs, contacts, play, copyright }) => {
  const address = contacts.list.filter(item => item.type === 'email')[0];

  return (
    <div className={cx('footer')}>
      <div className={cx('block')}>
        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={(90)}
          duration={600}
          translateY={40}
          animationId="workWithUs"
        >
          <div
            className={cx('title')}
            dangerouslySetInnerHTML={{ __html: workWithUs.title }}
          />
        </BlockAnimation>

        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={160}
          duration={700}
          translateY={30}
          animationId="workWithUsDescription"
        >
          <Text
            className={cx('description')}
            text={workWithUs.description}
          />
        </BlockAnimation>

        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={230}
          duration={700}
          translateY={30}
          animationId="workWithUsButton"
        >
          <Button.Base
            buttonText={workWithUs.buttonText}
            onClick={() => {
              window.location.href = `mailto:${address.value}`;
            }}
          />
        </BlockAnimation>
      </div>

      <div className={cx('block')}>
        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={(180)}
          duration={700}
          translateY={30}
          animationId="contacts"
        >
          <div
            className={cx('title')}
            dangerouslySetInnerHTML={{ __html: contacts.title }}
          />
        </BlockAnimation>

        <BlockAnimation
          play={play}
          skewY={-0.5}
          delay={230}
          duration={700}
          translateY={30}
          animationId="contactsList"
        >
          <ul className={cx('contactsList')}>
            {contacts.list.map(item => (
              <li key={item.type}>
                {item.type === 'email' && (
                  <React.Fragment>
                    <Text
                      text={`${item.name}:&nbsp`}
                    />
                    <Text
                      className={cx('textColor')}
                      text={`<a href='mailto:${item.value}'>${item.value}</a>`}
                    />
                  </React.Fragment>
                )}

                {item.type === 'text' && (
                  <React.Fragment>
                    <Text
                      text={`${item.name}:&nbsp`}
                    />
                    <Text
                      className={cx('textColor')}
                      text={item.value}
                    />
                  </React.Fragment>
                )}

                {item.type === 'link' && (
                  <Text
                    className={cx('textLink')}
                    text={`<a target="_blank" href='${item.value}'>${item.name}</a>`}
                  />
                )}

                {item.type === 'button' && (
                  <Button.Base
                    target="_blank"
                    link={item.value}
                    buttonText={item.name}
                  />
                )}
              </li>
            ))}
          </ul>
        </BlockAnimation>
      </div>

      <div className={cx('copyright')}>
        <Text text={copyright} />
      </div>
    </div>
  );
};

Footer.propTypes = {
  copyright: PropTypes.string,
  workWithUs: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    buttonUrl: PropTypes.string,
  }),
  contacts: PropTypes.shape({
    title: PropTypes.string,
    list: PropTypes.array,
  }),
  play: PropTypes.bool,
};

export default Footer;
