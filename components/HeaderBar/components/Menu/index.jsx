import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'

// Components
import { BlockAnimation } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

// Data
import base from 'mockData/base';

const Menu = () => {
  const router = useRouter();
  const { pathname } = router;

  const [isActiveAnimation, useSetActiveAnimation] = useState(false);
  useEffect(() => {
    useSetActiveAnimation(true);
  }, []);

  return (
    <div className={cx('modal')}>
      <ul className={cx('menuList')}>
        {base.menu.map((item, index) => (
          <li key={index}>
            <BlockAnimation
              play={isActiveAnimation}
              delay={40}
              duration={300*(index+1)}
              translateY={20}
              animationId={`menuList-${index}`}
            >
              <a
                href={item.link}
                className={cx('menuLink', (`${pathname}` === item.link ? 'active': null))}
              >
                {item.name}
              </a>
            </BlockAnimation>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Menu;
