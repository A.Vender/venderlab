import React, { useState } from 'react';

// Components
import Link from 'next/link';
import { Logo, Button, Picture } from 'components';
import Menu from './components/Menu';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const HeaderBar = () => {
  const [isMenuModalOpen, useSetOpenModal] = useState(false);
  const isActiveMenu = isMenuModalOpen ? 'active' : 'disactive';

  return (
    <React.Fragment>
      <div className={cx('headerBar')}>
        <Link href="/">
          <a className={cx('logoLink')}><Logo /></a>
        </Link>

        <Button.Empty
          className={cx('menu')}
          onClick={() => { useSetOpenModal(!isMenuModalOpen); }}
        >
          <Picture
            src={`/static/assets/svg/menu/${isActiveMenu}.svg`}
          />
        </Button.Empty>
      </div>

      {isMenuModalOpen && (<Menu />)}
    </React.Fragment>
  );
};

export default HeaderBar;
