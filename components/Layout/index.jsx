import React from 'react';

// Styles
import styles from './styles.less';

const Layout = ({ children }) => {
  return (
    <main>{children}</main>
  )
};

export default Layout;
