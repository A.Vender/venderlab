import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import { Button } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

class Tabs extends Component {
  state = {
    activeIndex: 0,
  };

  componentDidMount = () => {
    const { defaultIndex } = this.state;

    if (defaultIndex) {
      this.setState({ activeIndex: defaultIndex });
    }
  };

  setActiveContainer = (index) => this.setState({
    activeIndex: index,
  });

  getActiveClass = (index) => (
    (index === this.state.activeIndex) ? 'active' : 'disactive'
  );

  render() {
    const {
      tabs,
      tabsListClassName,
      containerClassName,
      children,
      onClick,
    } = this.props;

    return (
      <React.Fragment>
        <ul className={cx('tabList', tabsListClassName)}>
          {tabs && tabs.map((tab, index) => (
            <li key={index}>
              <Button.Empty
                className={cx('tab', this.getActiveClass(index))}
                onClick={() => {
                  this.setActiveContainer(index);
                  onClick(index, tab)
                }}
              >
                {tab.name}
              </Button.Empty>
            </li>
          ))}
        </ul>
        {children && children.map((item, index) => (
          <div key={index} className={cx('container', this.getActiveClass(index),  containerClassName)}>
            {item}
          </div>
        ))}
      </React.Fragment>
    );
  }
}

Tabs.defaultProps = {
  onClick: () => {},
};

Tabs.propTypes = {
  tabs: PropTypes.array.isRequired,
  children: PropTypes.array.isRequired,
  containerClassName: PropTypes.string,
  tabsListClassName: PropTypes.string,
  defaultIndex: PropTypes.number,
};

export default Tabs;
