import React, { Component } from 'react';
import PropTypes from 'prop-types';
import anime from "animejs";

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

/**
 * Компонент плавной отрисовки и плавного скрытия текста;
 */
class Sunny extends Component {
  state = {
    isComplete: false,
  };

  basicTimeline = null;

  componentDidMount() {
    this.replaceText();
    this.animStart();
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (!this.state.isComplete && nextProps.play) {
      this.basicTimeline.play();
      this.setState({ isComplete: true });
    }
  }

  animStart = () => {
    const { animationId, delay, duration } = this.props;

    this.basicTimeline = anime.timeline({
      autoplay: false,
      complete: () => this.props.callBack,
    })
      .add({
        targets: `#${animationId} .letter`,
        scale: [1.8,1],
        opacity: [0,1],
        translateZ: 0,
        easing: "easeOutExpo",
        duration: duration,
        delay: (el, i) => delay*i
      })
  };

  replaceText = () => {
    this.animationNode.innerHTML = this.animationNode.textContent
      .replace(/\S/g, "<span class='letter'>$&</span>");
  };

  render() {
    const { text, className, animationId } = this.props;

    return (
      <div
        id={animationId}
        className={cx(['text', className])}
        ref={animationNode => { this.animationNode = animationNode; }}
      >
        {text}
      </div>
    );
  }
}

Sunny.defaultProps = {
  play: false,
  delay: 90,
  duration: 950,
};

Sunny.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  animationId: PropTypes.string,
  callBack: PropTypes.func,
  play: PropTypes.bool,
  duration: PropTypes.number,
  delay: PropTypes.number,
};

export default Sunny;
