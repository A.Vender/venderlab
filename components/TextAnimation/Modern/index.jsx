import React, { Component } from 'react';
import PropTypes from 'prop-types';
import anime from 'animejs';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

/**
 * Компонент плавной отрисовки и плавного скрытия текста;
 */
class Modern extends Component {
  componentDidMount() {
    const { type } = this.props;

    this.replaceText();

    switch(type) {
      case 'inOut': this.animInOutStart(); break;
      case 'in': this.animInStart(); break;
      case 'out': this.animOutStart(); break;
    }
  }

  delay = (time) => (el, i) => time + 30 * i;

  in = {
    targets: `.${this.props.animationName} .letter`,
    translateX: [0,-30],
    opacity: [1,0],
    easing: "easeInExpo",
    duration: 1100,
    delay: this.delay(500),
  };

  out = {
    targets: `.${this.props.animationName} .letter`,
    translateX: [40,0],
    translateZ: 0,
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 1200,
    delay: this.delay(100),
  };

  setCallback = () => {
    let timer = null;
    timer = setTimeout(() => {
      this.props.callback();
      clearTimeout(timer);
    }, 600);
  };

  animInStart = () => anime.timeline({
    autoplay: this.props.autoplay,
    complete: () => this.setCallback(),
  }).add(this.in);

  animOutStart = () => anime.timeline({
    autoplay: this.props.autoplay,
    complete: () => this.setCallback(),
  }).add(this.out);

  animInOutStart = () => anime.timeline({
    autoplay: this.props.autoplay,
    complete: () => this.setCallback(),
  }).add(this.out).add(this.in);

  replaceText = () => {
    this.animation.innerHTML = this.animation.textContent
      .replace(/\S/g, "<span class='letter'>$&</span>");
  };

  render() {
    const { className, text, animationName } = this.props;

    return (
      <div
        className={`${animationName} ${className || ''} ${cx('text')}`}
        ref={nodeAnimation => { this.animation = nodeAnimation }}
      >
        {text}
      </div>
    );
  }
}

Modern.defaultProps = {
  autoplay: true,
  type: 'inOut',
  callback: () => {},
  animationName: 'animation'
};

Modern.propTypes = {
  className: PropTypes.string,
  animationName: PropTypes.string,
  text: PropTypes.string,
  callback: PropTypes.func,
  autoplay: PropTypes.bool,
  type: PropTypes.string,
};

export default Modern;
