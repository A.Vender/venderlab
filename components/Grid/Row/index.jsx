import React from 'react';
import PropTypes from 'prop-types';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Row = ({ children, className }) => (
  <ul className={cx('row', className)}>
    {children.map((item, key) => (
      <li key={key}>{item}</li>
    ))}
  </ul>
);

Row.propTypes = {
  children: PropTypes.array,
  className: PropTypes.string,
};

export default Row;
