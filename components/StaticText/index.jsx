import React from "react";

export default {
  projectName: 'VENDERLAB',
  details: 'Подробнее',
  scroll: 'Узнать больше',
  casesPageTitle: 'Кейсы',
  servicesPageTitle: 'Услуги',
  aboutPageTitle: 'О нас',
  tariffsPageTitle: 'Тарифы',
  contactsPageTitle: 'Контакты'
}
