import React from 'react';
import PropTypes from 'prop-types';

// Components
import { Title, Text, Button } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const ProductCard = ({ title, params, price, index, buttonName, paramsSubTitle }) => {
  const getNumber = (idx) => (idx < 10 ? `0${idx}.` : `${idx}.`);

  return (
    <div className={cx('productCard')}>
      <div className={cx('number')}>{getNumber(index)}</div>
      <Title level={3} className={cx('title')}>{title}</Title>

      {paramsSubTitle && (
        <div className={cx('paramSubtitle')}>{paramsSubTitle}</div>
      )}

      {params && (
        <ul className={cx('params')}>
          {params.map((item, index) => (
            <li key={index}>
              <Text className={cx('paramText')} text={item} />
            </li>
          ))}
        </ul>
      )}

      {price && (
        <div className={cx('price')}>
          {price}<span> ₽</span>
        </div>
      )}

      {buttonName && (
        <Button.Base
          link='/contacts'
          buttonText={buttonName}
          className={cx('cardButton')}
        />
      )}
    </div>
  );
};

ProductCard.propTypes = {
  title: PropTypes.string,
  params: PropTypes.array,
  price: PropTypes.string,
  index: PropTypes.number,
};

export default ProductCard;
