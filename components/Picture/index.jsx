import React, { Component } from 'react';
import PropTypes from 'prop-types';

/**
 * Компонент переключет картинки между desktop и mobile.
 * Если передать source будет использует в себе tag <picture/>, что
 * дает гибкость в использовании при адаптиве;
 * ----------------------------------------------------------------
 * ⬇︎⬇︎⬇︎ Способ применения симореть в конце файла ⬇︎⬇︎⬇︎
 */
class Picture extends Component {
  renderImage() {
    const { lazy, alt, className, src, title } = this.props;

    return (
      <img
        loading={lazy && 'lazy'}
        className={className}
        title={title}
        src={src}
        alt={alt}
      />
    );
  }

  render() {
    const { source } = this.props;

    if (source) {
      return (
        <picture>
          {source.map((item, index) => (
            <source
              key={`source-${index}`}
              srcSet={item.srcSet}
              media={`(${item.media})`}
            />
          ))}
          {this.renderImage()}
        </picture>
      );
    }

    return (
      this.renderImage()
    );
  }
}

Picture.defaultProps = {
  title: '',
  alt: '',
  lazy: false,
};

Picture.propTypes = {
  src: PropTypes.string,
  alt: PropTypes.string,
  title: PropTypes.string,
  lazy: PropTypes.bool,
  source: PropTypes.arrayOf(
    PropTypes.shape({
      srcSet: PropTypes.string.isRequired,
      media: PropTypes.string,
    }),
  ),
  className: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
  ]),
};

export default Picture;


/**
 Способ применения
 ------------------------------
 <Picture
   src={webImage}
   className={cx('bgImage')}
   alt="Flowers"
   source={[{
     srcSet: mobImage,
     media: 'max-width: 768px',
   }]}
 />

 <Picture
   src={imageSrc}
   alt="image_from_static"
 />
 */
