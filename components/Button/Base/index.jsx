import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';

// Components
import Empty from '../Empty';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Base = ({ onClick, buttonText, type, className, link, target }) => {
  if (link) {
    return (
      <a href={link} target={target} className={cx('button', type, className)}>{buttonText}</a>
    );
  }

  return (
    <Empty
      className={cx('button', type, className)}
      onClick={onClick}
    >
      {buttonText}
    </Empty>
  );
};

Base.defaultProps = {
  type: 'border',
  link: null,
};

Base.propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  buttonText: PropTypes.string,
  onClick: PropTypes.func,
};

export default Base;
