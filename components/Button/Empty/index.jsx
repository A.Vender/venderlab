import React from 'react';
import PropTypes from 'prop-types';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Empty = ({
  className,
  children,
  onClick,
}) => (
  <button
    className={cx('button', className)}
    onClick={onClick}
  >{children}</button>
);

Empty.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
};

export default Empty;
