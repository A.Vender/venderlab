import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';

const Meta = ({ title, description, keywords }) => {
  const router = useRouter();

  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />

      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content="/static/assets/logo_image.png" />
      <meta property="og:locale" content="ru_RU" />
      <meta property="og:site_name" content="VENDERLAB Россия" />

      <meta property="og:url" content={router.asPath} />
      <link rel="shortcut icon" href="/static/assets/favicon.svg" />
      <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no,viewport-fit=cover" />

      <meta name="author" content="VENDERLAB" />
      <meta name="publisher" content="VENDERLAB" />
      <link rel="canonical" href={router.asPath} />

      <meta name="yandex-verification" content="489094919403ed56" />
      <meta name="google-site-verification" content="8JxkgaXnwO7edlhe6zCwiciQgh1LIGplruvrCHlbnpA" />
    </Head>
  )
};

export default Meta;
