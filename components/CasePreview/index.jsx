import React from 'react';
import PropTypes from 'prop-types';

// Components
import { Picture, Title, Button, Text } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const CasePreview = ({
  title,
  link,
  target,
  description,
  buttonName,
  preview,
  index,
}) => {
  const getNumber = (idx) => (idx < 10 ? `0${idx}.` : `${idx}.`);

  return (
    <div className={cx('casePreview')}>
      <div className={cx('number')}>{getNumber(index)}</div>

      <div className={cx('preview')}>
        <Picture
          lazy={false}
          src={preview}
          alt={title}
        />
      </div>

      <div className={cx('wrapper')}>
        <div>
          <Title level={3} className={cx('title')}>{title}</Title>
          {description && (
            <Text text={description} />
          )}
        </div>
        <Button.Base
          type="bborder"
          link={link}
          target={target}
          className={cx('button')}
          buttonText={buttonName}
        />
      </div>
    </div>
  );
};

CasePreview.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  buttonName: PropTypes.string,
  preview: PropTypes.string,
  target: PropTypes.string,
  link: PropTypes.string,
  index: PropTypes.number,
};

export default CasePreview;
