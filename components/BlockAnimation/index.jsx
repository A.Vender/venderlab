import React, { Component } from 'react';
import PropTypes from 'prop-types';
import anime from "animejs";

class BlockAnimation extends Component {
  state = {
    isComplete: false,
  };

  basicTimeline = null;

  componentDidMount() {
    this.animStart();
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (!this.state.isComplete && nextProps.play) {
      this.basicTimeline.play();
      this.setState({ isComplete: true });
    }
  }

  animStart = () => {
    const { animationId, delay, duration, skewY } = this.props;

    this.basicTimeline = anime.timeline({
      autoplay: false,
      complete: () => this.props.callBack,
    })
      .add({
        targets: `#${animationId}`,
        skewX: [skewY,0],
        opacity: [0,1],
        translateY: 0,
        easing: "easeInOutSine",
        duration,
        delay,
      })
  };

  render() {
    const { animationId, children, translateY, className } = this.props;

    return (
      <div
        id={animationId}
        className={className}
        style={{
          transform: `translateY(${translateY}px)`
        }}
      >
        {children}
      </div>
    );
  }
}

BlockAnimation.defaultProps = {
  play: false,
  delay: 25,
  duration: 600,
  translateY: 90,
  skewY: -2
};

BlockAnimation.propTypes = {
  className: PropTypes.string,
  animationId: PropTypes.string,
  translateY: PropTypes.number,
  skewY: PropTypes.number,
  callBack: PropTypes.func,
  play: PropTypes.bool,
  duration: PropTypes.number,
  delay: PropTypes.number,
};

export default BlockAnimation;
