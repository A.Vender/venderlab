const base = {
  heading: {
    subTitle: 'Креативная веб-студия VENDERLAB',
    title: 'Создадим сайт для вашего бизнеса под ключ. <span>Веб-дизайн, разработка, настройка SEO, техподдержка</span>',
    description: 'Используем нативный код и SEO-инструменты',
    buttonService: {
      name: 'Кейсы',
      link: '/cases'
    },
    buttonOrder: {
      name: 'Обсудить проект',
      link: 'https://forms.gle/NjjgjgtogSiyvSLs6'
    }
  },
  title: 'VENDERLAB — с заботой о вашем бизнесе',
  textAbout: '<b>Кто мы?</b> Креативная команда профессионалов с 10-летним опытом работы в диджитал-проектах таких крупных компаний, как Rambler Group, TVZAVR, «Афиша.Рестораны», «Дочки-сыночки», beeline.<br/><br/><b>Наши сильные стороны</b>: программирование, веб-дизайн, копирайтинг, маркетинговые исследования. Все это позволяет нам создавать крутые проекты, среди которых может оказаться и ваш. <br/><br/><b>Воплотим вашу идею сайта</b> как на уровне дизайна, так и на уровне разработки — наши программисты оживят ваш проект и выложат на отдельный домен. <br/><br/><b>Нужно текстовое наполнение?</b> Наши копирайтеры легко с этим справятся. <br/><b>Думаете, как обойти конкурентов?</b> Поможем с анализом рынка. <br/><b>С чего начать?</b> Свяжитесь с нами, чтобы обсудить проект.',
  menu: [
    {
      name: 'Главная',
      link: '/'
    },
    {
      name: 'О нас',
      link: '/about'
    },
    {
      name: 'Услуги',
      link: '/services'
    },
    {
      name: 'Кейсы',
      link: '/cases'
    },
    {
      name: 'Тарифы',
      link: '/tariffs'
    },
    {
      name: 'Контакты',
      link: '/contacts'
    }
  ],
  legal: {
    title: 'Юридическая информация',
    list: [{
      type: 'text',
      name: 'ИП',
      value: 'Вендер Александр Константинович'
    }, {
      type: 'text',
      name: 'ИНН',
      value: '911113290543'
    }, {
      type: 'text',
      name: 'ОГРНИП',
      value: '321774600221816'
    }, {
      type: 'link',
      name: 'Договор на оказание услуг',
      value: 'https://drive.google.com/file/d/1jLpbSLG6gpLThpC5HWOTzmh4r4KjrkeU/view?usp=sharing'
    }]
  },
  footer: {
    copyright: "Все права защищены. Полное или частичное копирование материалов Сайта запрещены. <br/>&copy;Venderlab, 2021 г.",
    workWithUs: {
      title: 'Не нашли подходящий <br/>пакет услуг?',
      description: 'Свяжитесь с нами, и мы подготовим для вас индивидуальное предложение.',
      buttonText: 'Написать нам на почту',
      buttonUrl: '/contacts'
    },
    contacts: {
      title: 'Контакты',
      list: [{
        type: 'text',
        name: 'Телефон',
        value: '+7 (985) 951-41-90, +7 (916) 042-87-23'
      }, {
        type: 'email',
        name: 'Почта',
        value: 'venderlab@gmail.com'
      }, {
        type: 'link',
        name: 'Юридическая информация',
        value: '/legal'
      }, {
        type: 'button',
        name: 'Обсудить проект',
        value: 'https://forms.gle/NjjgjgtogSiyvSLs6'
      }]
    },
    social: [{
      name: 'instagram',
      link: 'https://instagram',
      icon: '/static/assets/svg/social/instagram.svg'
    }, {
      name: 'whatsUp',
      link: '#',
      icon: '/static/assets/svg/social/whatsup-white.svg'
    }]
  }
};

export default base;
