const cases = {
  tabs: [
    {
      name: "Дизайн",
      alias: "design"
    },
    {
      name: "Сайты",
      alias: "site"
    }
  ],
  entries: {
    design: [
      {
        seo: {
          title: 'Веб-дизайн сайта для онлайн-школы – кейс веб-студии VENDERLAB',
          description: 'Разработка веб-дизайна сайта для онлайн-школы – пример работы от веб-студии Venderlab (Вендерлаб). Адекватные цены, стильный современный дизайн.',
          keywords: 'веб-дизайна сайта для онлайн-школы, кейсы, портфолио, примеры работы'
        },
        id: "1",
        title: "Interns",
        description: "Дизайн сайта для онлайн-школы",
        buttonName: "Узнать больше",
        link: "/case/1",
        next: "/case/2",
        nextButtonText: "Следующий кейс",
        preview: "/static/assets/png/interns/preview.png",
        images: [
          "/static/assets/png/interns/image_1.png",
          "/static/assets/png/interns/image_2.png",
          "/static/assets/png/interns/image_3.png",
          "/static/assets/png/interns/image_4.png",
          "/static/assets/png/interns/image_5.png",
          "/static/assets/png/interns/image_6.png",
          "/static/assets/png/interns/image_7.png",
          "/static/assets/png/interns/image_8.png",
          "/static/assets/png/interns/image_9.png"
        ]
      },
      {
        seo: {
          title: 'Веб-дизайн сайта для фотографа – кейс веб-студии VENDERLAB',
          description: 'Разработка прототипа сайта для фешн-фотографа – пример работы от веб-студии Venderlab (Вендерлаб). Адекватные цены, стильный современный дизайн.',
          keywords: 'веб-дизайн и разработка сайта, прототип сайта для фотографа, кейсы, портфолио, примеры работы\n'
        },
        id: "2",
        title: "Street Style",
        description: "Прототип сайта для fashion-фотографа",
        buttonName: "Узнать больше",
        link: "/case/2",
        next: "/case/3",
        nextButtonText: "Следующий кейс",
        preview: "/static/assets/png/street-style/preview.png",
        images: [
          "/static/assets/png/street-style/image_1.png",
          "/static/assets/png/street-style/image_2.png",
          "/static/assets/png/street-style/image_3.png",
          "/static/assets/png/street-style/image_4.png",
          "/static/assets/png/street-style/image_5.png",
          "/static/assets/png/street-style/image_6.png",
          "/static/assets/png/street-style/image_7.png",
          "/static/assets/png/street-style/image_8.png",
          "/static/assets/png/street-style/image_9.png",
          "/static/assets/png/street-style/image_10.png"
        ]
      },
      {
        seo: {
          title: 'Веб-дизайн сайта для фудкорта – кейс веб-студии VENDERLAB',
          description: 'Разработка прототипа сайта для фудкорта – пример работы от веб-студии Venderlab (Вендерлаб). Адекватные цены, стильный современный дизайн.',
          keywords: 'веб-дизайн и разработка сайта, прототип сайта для фудкорта, кейсы, портфолио, примеры работы'
        },
        id: "3",
        title: "Easy Food Сourt",
        description: "Прототип сайта для фудкорта",
        buttonName: "Узнать больше",
        link: "/case/3",
        next: "/case/4",
        nextButtonText: "Следующий кейс",
        preview: "/static/assets/png/easy-food/preview.png",
        images: [
          "/static/assets/png/easy-food/image_1.png",
          "/static/assets/png/easy-food/image_2.png",
          "/static/assets/png/easy-food/image_3.png",
          "/static/assets/png/easy-food/image_4.png",
          "/static/assets/png/easy-food/image_5.png",
          "/static/assets/png/easy-food/image_6.png"
        ]
      },
      {
        seo: {
          title: 'Веб-дизайн сайта для студии красоты – кейс веб-студии VENDERLAB',
          description: 'Разработка прототипа мобильной и веб-версии сайта для студии красоты – пример работы от веб-студии Venderlab (Вендерлаб). Адекватные цены, стильный современный дизайн.',
          keywords: 'веб-дизайн и разработка сайта, прототип сайта для студии красоты, кейсы, портфолио, примеры работы'
        },
        id: "4",
        title: "MakeUp Studio",
        description: "Прототип сайта для студии красоты. Для мобильной и web-версий",
        buttonName: "Узнать больше",
        link: "/case/4",
        next: "/case/5",
        copyright: [
          "<a target='_blank' href='http://www.freepik.com'>Designed by master1305 / Freepik</a>"
        ],
        nextButtonText: "Следующий кейс",
        preview: "/static/assets/png/make-up-studio/preview.png",
        images: [
          "/static/assets/png/make-up-studio/image_1.png",
          "/static/assets/png/make-up-studio/image_2.png",
          "/static/assets/png/make-up-studio/image_3.png",
          "/static/assets/png/make-up-studio/image_4.png",
          "/static/assets/png/make-up-studio/image_5.png"
        ]
      },
      {
        seo: {
          title: 'Веб-дизайн сайта для фотографа – кейс веб-студии VENDERLAB',
          description: 'Разработка прототипа сайта для свадебного и тревел-фотографа – пример работы от веб-студии Venderlab (Вендерлаб). Адекватные цены, стильный современный дизайн.',
          keywords: 'веб-дизайн и разработка сайта, прототип сайта для фотографа, кейсы, портфолио, примеры работы'
        },
        id: "5",
        title: "Photographer",
        description: "Прототип сайта для фотографа широкого профиля (портретные и свадебные фотосессии, travel-съемка)",
        buttonName: "Узнать больше",
        link: "/case/5",
        next: "/case/6",
        nextButtonText: "Следующий кейс",
        preview: "/static/assets/png/photographer/preview.png",
        images: [
          "/static/assets/png/photographer/image_1.png",
          "/static/assets/png/photographer/image_2.png",
          "/static/assets/png/photographer/image_3.png",
          "/static/assets/png/photographer/image_4.png"
        ]
      },
      {
        seo: {
          title: 'Веб-дизайн сайта для собачьего питомника – кейс веб-студии VENDERLAB',
          description: 'Разработка прототипа сайта для питомника французских бульдогов – пример работы от веб-студии Venderlab (Вендерлаб). Адекватные цены, стильный современный дизайн.',
          keywords: 'веб-дизайн и разработка сайта, прототип сайта для собачьего питомника, кейсы, портфолио, примеры работы'
        },
        id: "6",
        title: "BullDog Club",
        description: "Прототип сайта для питомника французских бульдогов",
        buttonName: "Узнать больше",
        link: "/case/7",
        next: "/case/1",
        nextButtonText: "Следующий кейс",
        preview: "/static/assets/png/bulldog-club/preview.png",
        images: [
          "/static/assets/png/bulldog-club/image_1.png",
          "/static/assets/png/bulldog-club/image_2.png",
          "/static/assets/png/bulldog-club/image_3.png",
          "/static/assets/png/bulldog-club/image_4.png",
          "/static/assets/png/bulldog-club/image_5.png"
        ]
      }
    ],
    site: [
      {
        id: "29012021",
        title: "MUR — АВТОРСКОЕ ИЗДАНИЕ О МОДЕ И ТРЕНДАХ",
        description: "Разработка сайта murmur.ru. Адаптация под web и mobile",
        buttonName: "Смотреть сайт",
        link: "https://murmur.ru/",
        target: "_blank",
        preview: "/static/assets/png/murmur/preview.png"
      }
    ],
    depricated_content: [{
      id: "12312",
      title: "Авто",
      description: "Уникальная рекламная статья для Škoda Auto, фотосъемка.",
      buttonName: "Узнать больше",
      link: "https://www.wmj.ru/stil-zhizni/avtomobili/ot-sportivnogo-oblika-do-vnushitelnoi-vmestitelnosti-izuchaem-skoda-kodiaq-sportline.htm",
      target: "_blank",
      preview: "/static/assets/png/articles/preview_1.png"
    }, {
      id: "23434",
      title: "Красота и здоровье",
      description: "Уникальный партнерский материал для бренда «Пантовигар».",
      buttonName: "Узнать больше",
      link: "https://www.passion.ru/beauty/volosy/seboreya-vypadenie-volos-i-eshe-4-opasnykh-priznaka-pri-kotorykh-rekomenduetsya-obratitsya-k-trikhologu.htm",
      target: "_blank",
      preview: "/static/assets/png/articles/preview_2.png"
    }, {
      id: "45234",
      title: "Мода",
      description: "Уникальная рекламная статья для бренда детской одежды Gulliver.",
      buttonName: "Узнать больше",
      link: "https://www.wmj.ru/moda/tendencii/vse-na-bort-kruiznyi-stil-dlya-samykh-malenkikh-yakhtsmenov.htm",
      target: "_blank",
      preview: "/static/assets/png/articles/preview_3.png"
    }]
  }
};

export default cases;
