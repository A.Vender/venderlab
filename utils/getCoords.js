export const getCoords = (elem) => {
  const { top, height } = elem.getBoundingClientRect();
  return (top + height) / (window.innerHeight + height);
};
