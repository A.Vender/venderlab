import { useEffect, useState } from 'react';

// Utils
import { getMobileOS } from 'utils/detectMobile';

/**
 * Функция отпределяет тип устройства и возвращает булевое значение;
 */
export const useIsMobile = (defaultVal = null) => {
  const [isMobile, setMobileOs] = useState(defaultVal);

  useEffect(() => {
    setMobileOs(!!getMobileOS());
  }, []);

  return isMobile;
};

/**
 * Функция отпределяет тип устройства и возвращает тип устройства в виде строки;
 */
export const useMobileOs = (defaultVal = null) => {
  const [mobileOs, setMobileOs] = useState(defaultVal);

  useEffect(() => {
    setMobileOs(getMobileOS());
  }, []);

  return mobileOs;
};
