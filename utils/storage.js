export const setSessionStorage = (key, value) => sessionStorage.setItem(key, value);
export const getFromSessionStorage = (key) => sessionStorage.getItem(key);
export const removeFromSessionStorage = () => sessionStorage.removeItem(key);
